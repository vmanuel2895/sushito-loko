import Swal from "sweetalert2";

export function alerta(icono:any,titulo:string,texto:string){
  Swal.fire({
    icon: icono,
    title: titulo,
    text: texto,
    showConfirmButton: false,
    timer: 15000,
    showClass: {
      popup: 'animate__animated animate__fadeInDown'
    },
    hideClass: {
      popup: 'animate__animated animate__fadeOutUp'
    },
    html:
        `${texto}<br>`+
        `<br><b>SOPORTE TÉCNICO</b>`+
        '<b><p style="text-align: center;">TELÉFONO: 735-418-20-83</p></b>',
  });
}

export function confirmarPedido(){
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })

  swalWithBootstrapButtons.fire({
    html:
        `<br><b>FINALIZAR</b>`+
        '<b><p style="text-align: center;">ORDEN</p></b>',
    showCancelButton: true,
    confirmButtonText: 'SI',
    cancelButtonText: 'NO',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {

    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {

    }
  })
}


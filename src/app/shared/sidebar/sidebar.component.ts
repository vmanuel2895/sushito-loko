import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar/sidebar.service';
import { sideBar } from '../../interfaces/sidebar.interface';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public side:sideBar[]= [];
  public nombre = '';

  constructor(private _sideBar:SidebarService,
              private _usuario:UsuarioService) {
                this.nombre = this._usuario.usuario.nombreUsuario;
              }

  ngOnInit(): void {
    this.side = this._sideBar.getSideBar();
  }

}

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  now: Date;

  constructor() { }

  ngOnInit() {
    this.now = new Date();
    setInterval(() => {
      this.now = new Date();
    }, 1000);
  }

}

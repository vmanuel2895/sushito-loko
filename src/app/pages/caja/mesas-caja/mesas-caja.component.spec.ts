import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesasCajaComponent } from './mesas-caja.component';

describe('MesasCajaComponent', () => {
  let component: MesasCajaComponent;
  let fixture: ComponentFixture<MesasCajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesasCajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesasCajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

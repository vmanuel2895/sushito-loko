import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FondoCajaComponent } from './fondo-caja.component';

describe('FondoCajaComponent', () => {
  let component: FondoCajaComponent;
  let fixture: ComponentFixture<FondoCajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FondoCajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FondoCajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

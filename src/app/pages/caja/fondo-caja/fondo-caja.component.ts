import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-fondo-caja',
  templateUrl: './fondo-caja.component.html',
  styleUrls: ['./fondo-caja.component.css']
})
export class FondoCajaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  guardar(){
    Swal.fire({
      icon: 'success',
      title: 'Fondo de caja guardado con éxito',
      showConfirmButton: false,
      timer: 1500
    })
  }

}

import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-propinas',
  templateUrl: './agregar-propinas.component.html',
  styleUrls: ['./agregar-propinas.component.css']
})
export class AgregarPropinasComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  guardar(){
    Swal.fire({
      icon: 'success',
      title: 'Propinas guardadas con éxito',
      showConfirmButton: false,
      timer: 1500
    })
  }

}

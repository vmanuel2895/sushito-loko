import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarPropinasComponent } from './agregar-propinas.component';

describe('AgregarPropinasComponent', () => {
  let component: AgregarPropinasComponent;
  let fixture: ComponentFixture<AgregarPropinasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarPropinasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarPropinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

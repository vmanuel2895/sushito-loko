import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-propinas',
  templateUrl: './propinas.component.html',
  styleUrls: ['./propinas.component.css']
})
export class PropinasComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit(): void {
  }

  async pass(){
    const { value: password } = await Swal.fire({
      title: 'Ingresa contraseña',
      input: 'password',
      inputPlaceholder: 'Contraseña',
      confirmButtonColor: '#1DAD47',
    })
    if (password) {
      this._router.navigateByUrl('/dashboard/agregar/egresos');
    }
  }

}

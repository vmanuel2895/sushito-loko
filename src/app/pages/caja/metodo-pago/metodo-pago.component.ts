import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-metodo-pago',
  templateUrl: './metodo-pago.component.html',
  styleUrls: ['./metodo-pago.component.css']
})
export class MetodoPagoComponent implements OnInit {

  public metodo = '';
  public data = {
    title:'',
    img:''
  }
  public estile;

  constructor(private _route:ActivatedRoute) { }

  ngOnInit(): void {
    this.metodo = this._route.snapshot.paramMap.get('metodo');
    this.imagen();
  }

  imagen(){
    this.estile = document.getElementById("metodo");
    if(this.metodo == 'efectivo'){
      this.data.img = '../../../../assets/images/sushito/efectivo.png'
      this.data.title = this.metodo.toLocaleUpperCase();
      this.estile.style.width = '163px'
      this.estile.style.height = '144px'
    }else if(this.metodo == 'credito' || this.metodo == 'debito'){
      this.data.img = '../../../../assets/images/sushito/tarjeta.png'
      this.data.title = 'TARJETA DE '+this.metodo.toLocaleUpperCase();
      this.estile.style.width = '300px'
      this.estile.style.height = '144px'
    }else{
      this.data.img = '../../../../assets/images/sushito/transfer.png'
      this.data.title = this.metodo.toLocaleUpperCase();
      this.estile.style.width = '130px'
      this.estile.style.height = '113px'
    }
  }

}

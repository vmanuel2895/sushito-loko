import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-egresos',
  templateUrl: './agregar-egresos.component.html',
  styleUrls: ['./agregar-egresos.component.css']
})
export class AgregarEgresosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  guardar(){
    Swal.fire({
      icon: 'success',
      title: 'Productos guardados con éxito',
      showConfirmButton: false,
      timer: 1500
    })
  }

}

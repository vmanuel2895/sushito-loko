import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarEgresosComponent } from './agregar-egresos.component';

describe('AgregarEgresosComponent', () => {
  let component: AgregarEgresosComponent;
  let fixture: ComponentFixture<AgregarEgresosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarEgresosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarEgresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

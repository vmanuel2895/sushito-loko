import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-egresos',
  templateUrl: './egresos.component.html',
  styleUrls: ['./egresos.component.css']
})
export class EgresosComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit(): void {
  }

  async pass(){
    const { value: password } = await Swal.fire({
      title: 'Ingresa contraseña',
      input: 'password',
      inputPlaceholder: 'Contraseña',
      confirmButtonColor: '#1DAD47',
    })
    if (password) {
      this._router.navigateByUrl('/dashboard/agregar/egresos');
    }
  }

}

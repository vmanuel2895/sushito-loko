import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-informacion-pago',
  templateUrl: './informacion-pago.component.html',
  styleUrls: ['./informacion-pago.component.css']
})
export class InformacionPagoComponent implements OnInit {

  constructor(private _router:Router,) { }

  ngOnInit(): void {
  }

  async pass(){
    const { value: password } = await Swal.fire({
      title: 'Ingresa contraseña',
      input: 'password',
      inputPlaceholder: 'Contraseña',
      confirmButtonColor: '#1DAD47',
    })

    if (password) {
      this.descuento();
    }
  }

  async descuento(){
    const { value: descuento } = await Swal.fire({
      title: 'Ingresa desccuento',
      input: 'text',
      inputPlaceholder: 'Descuento',
      confirmButtonColor: '#1DAD47',
    })

    if (descuento) {
      console.log(descuento);

    }
  }

  pagar(){
    this._router.navigateByUrl('/dashboard/metodos/pago');
  }

}

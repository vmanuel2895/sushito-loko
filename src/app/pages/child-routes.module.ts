import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';

//MESERO
import { MesasComponent } from './mesero/mesas/mesas.component';
import { VerMesaComponent } from './mesero/ver-mesa/ver-mesa.component';
import { MenuComponent } from './mesero/menu/menu.component';
import { PedidosComponent } from './mesero/pedidos/pedidos.component';
import { PromocionesComponent } from './mesero/promociones/promociones.component';

//CAJA
import { MesasCajaComponent } from './caja/mesas-caja/mesas-caja.component';
import { InformacionPagoComponent } from './caja/informacion-pago/informacion-pago.component';
import { MetodosPagoComponent } from './caja/metodos-pago/metodos-pago.component';
import { MetodoPagoComponent } from './caja/metodo-pago/metodo-pago.component';
import { CorteCajaComponent } from './caja/corte-caja/corte-caja.component';
import { EgresosComponent } from './caja/egresos/egresos.component';
import { AgregarEgresosComponent } from './caja/egresos/agregar-egresos/agregar-egresos.component';
import { FondoCajaComponent } from './caja/fondo-caja/fondo-caja.component';
import { PropinasComponent } from './caja/propinas/propinas.component';
import { AgregarPropinasComponent } from './caja/propinas/agregar-propinas/agregar-propinas.component';
import { NuevoClienteComponent } from './caja/nuevo-cliente/nuevo-cliente.component';
import { DatosFacturacionComponent } from './caja/nuevo-cliente/datos-facturacion/datos-facturacion.component';

import { AdminGuard } from '../guards/admin.guard';


const childRoutes: Routes = [
  { path: '', component: DashboardComponent, data: { titulo: 'Dashboard' } },
  //MESERO
  { path: 'mesas', component: MesasComponent, data: { titulo: 'Mesas' }},
  { path: 'mesa', component: VerMesaComponent, data: { titulo: 'Mesa' }},
  { path: 'menu', component: MenuComponent, data: { titulo: 'Menu' }},
  { path: 'pedidos', component: PedidosComponent, data: { titulo: 'Pedidos' }},
  { path: 'promociones', component: PromocionesComponent, data: { titulo: 'Promociones' }},
  //CAJERO
  { path: 'caja', component: MesasCajaComponent, data: { titulo: 'Caja' }},
  { path: 'informacion/pago', component: InformacionPagoComponent, data: { titulo: 'Informacion pago' }},
  { path: 'metodos/pago', component: MetodosPagoComponent, data: { titulo: 'Metodos de pago' }},
  { path: 'metodo/pago/:metodo', component: MetodoPagoComponent, data: { titulo: 'Metodos de pago' }},
  { path: 'corte/caja', component: CorteCajaComponent, data: { titulo: 'Corte de caja' }},
  { path: 'egresos', component: EgresosComponent, data: { titulo: 'Egresos' }},
  { path: 'agregar/egresos', component: AgregarEgresosComponent, data: { titulo: 'Agregar egresos' }},
  { path: 'fondo', component: FondoCajaComponent, data: { titulo: 'Fondo de caja' }},
  { path: 'propinas', component: PropinasComponent, data: { titulo: 'Propinas' }},
  { path: 'agregar/propinas', component: AgregarPropinasComponent, data: { titulo: 'Agregar propinas' }},
  { path: 'agregar/cliente', component: NuevoClienteComponent, data: { titulo: 'Agregar cliente nuevo' }},
  { path: 'datos/facturacion', component: DatosFacturacionComponent, data: { titulo: 'Agregar datos de facturacion' }},
  // Rutas de Admin
]



@NgModule({
  imports: [ RouterModule.forChild(childRoutes) ],
  exports: [ RouterModule ]
})
export class ChildRoutesModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerMesaComponent } from './ver-mesa.component';

describe('VerMesaComponent', () => {
  let component: VerMesaComponent;
  let fixture: ComponentFixture<VerMesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerMesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerMesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Modulos
import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';

import { PipesModule } from '../pipes/pipes.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';

//MESERO
import { MesasComponent } from './mesero/mesas/mesas.component';
import { VerMesaComponent } from './mesero/ver-mesa/ver-mesa.component';
import { MenuComponent } from './mesero/menu/menu.component';
import { PedidosComponent } from './mesero/pedidos/pedidos.component';
import { PromocionesComponent } from './mesero/promociones/promociones.component';

//CAJA
import { MesasCajaComponent } from './caja/mesas-caja/mesas-caja.component';
import { InformacionPagoComponent } from './caja/informacion-pago/informacion-pago.component';
import { MetodosPagoComponent } from './caja/metodos-pago/metodos-pago.component';
import { MetodoPagoComponent } from './caja/metodo-pago/metodo-pago.component';
import { CorteCajaComponent } from './caja/corte-caja/corte-caja.component';
import { EgresosComponent } from './caja/egresos/egresos.component';
import { AgregarEgresosComponent } from './caja/egresos/agregar-egresos/agregar-egresos.component';
import { FondoCajaComponent } from './caja/fondo-caja/fondo-caja.component';
import { PropinasComponent } from './caja/propinas/propinas.component';
import { AgregarPropinasComponent } from './caja/propinas/agregar-propinas/agregar-propinas.component';
import { NuevoClienteComponent } from './caja/nuevo-cliente/nuevo-cliente.component';
import { DatosFacturacionComponent } from './caja/nuevo-cliente/datos-facturacion/datos-facturacion.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PagesComponent,
    MesasComponent,
    VerMesaComponent,
    MenuComponent,
    PedidosComponent,
    PromocionesComponent,
    MesasCajaComponent,
    InformacionPagoComponent,
    MetodosPagoComponent,
    MetodoPagoComponent,
    CorteCajaComponent,
    EgresosComponent,
    AgregarEgresosComponent,
    FondoCajaComponent,
    PropinasComponent,
    AgregarPropinasComponent,
    NuevoClienteComponent,
    DatosFacturacionComponent
  ],
  exports: [
    DashboardComponent,
    PagesComponent,
    MesasComponent,
    VerMesaComponent,
    MenuComponent,
    PedidosComponent,
    PromocionesComponent,
    MesasCajaComponent,
    InformacionPagoComponent,
    MetodosPagoComponent,
    MetodoPagoComponent,
    CorteCajaComponent,
    EgresosComponent,
    AgregarEgresosComponent,
    FondoCajaComponent,
    PropinasComponent,
    AgregarPropinasComponent,
    NuevoClienteComponent,
    DatosFacturacionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule,
    ComponentsModule,
    PipesModule
  ]
})
export class PagesModule { }

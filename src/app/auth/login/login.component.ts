import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from "../../services/login/login.service";
import Swal from 'sweetalert2'
import { alerta } from 'src/app/functions/alertas';
import { UsuarioService } from 'src/app/services/usuario.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent {

  public formSubmitted = false;

  public loginForm = this._fb.group({
    nombre:['', Validators.required],
    password:['', Validators.required]
  })

  constructor(private _router:Router,
              private _login:UsuarioService,
              private _fb:FormBuilder) { }

  login(){
    this._login.login(this.loginForm.value).subscribe(resp =>{
      this._router.navigateByUrl('/');
    },(err) => {
      alerta('error', 'ERROR' ,err.error.msg.toUpperCase());
      this.loginForm.reset();
    })
  }

  campoNoValido(campo:string):boolean{
    if (this.loginForm.get(campo).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

}

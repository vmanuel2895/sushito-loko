import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css']
})
export class DetalleProductoComponent implements OnInit {

  public contador = 1;
  public activar = false;
  @Input() activarCarrito = true;

  constructor() { }

  ngOnInit(): void {
    /* this.contador = 1;
    this.boton('pan'); */
  }

  boton(item:any){
    if(this.contador != 1){
      document.getElementById(item).style.height="auto"
      document.getElementById('imagen').style.height="90px"
      if(this.activarCarrito == true){
        this.activar = false;
      }
      this.contador = 1;
    }else{
      document.getElementById(item).style.height="350px"
      document.getElementById('imagen').style.height="200px"
      if(this.activarCarrito == true){
        this.activar = true;
      }

      this.contador = 0;
    }
  }

}

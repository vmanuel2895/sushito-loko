import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleOrdenesComponent } from './detalle-ordenes.component';

describe('DetalleOrdenesComponent', () => {
  let component: DetalleOrdenesComponent;
  let fixture: ComponentFixture<DetalleOrdenesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleOrdenesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleOrdenesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

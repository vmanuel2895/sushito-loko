import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleOrdenInfoComponent } from './detalle-orden-info.component';

describe('DetalleOrdenInfoComponent', () => {
  let component: DetalleOrdenInfoComponent;
  let fixture: ComponentFixture<DetalleOrdenInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleOrdenInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleOrdenInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

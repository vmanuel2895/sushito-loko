import { Component, OnInit } from '@angular/core';
import { confirmarPedido } from 'src/app/functions/alertas';

@Component({
  selector: 'app-detalle-orden-info',
  templateUrl: './detalle-orden-info.component.html',
  styleUrls: ['./detalle-orden-info.component.css']
})
export class DetalleOrdenInfoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  finalizar(){
    confirmarPedido();
  }

}

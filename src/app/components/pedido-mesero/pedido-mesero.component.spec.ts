import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoMeseroComponent } from './pedido-mesero.component';

describe('PedidoMeseroComponent', () => {
  let component: PedidoMeseroComponent;
  let fixture: ComponentFixture<PedidoMeseroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoMeseroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoMeseroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pedido-mesero',
  templateUrl: './pedido-mesero.component.html',
  styleUrls: ['./pedido-mesero.component.css']
})
export class PedidoMeseroComponent implements OnInit {

  @Input() title = '';

  constructor() { }

  ngOnInit(): void {
  }

}

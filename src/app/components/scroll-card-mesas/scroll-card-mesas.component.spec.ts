import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollCardMesasComponent } from './scroll-card-mesas.component';

describe('ScrollCardMesasComponent', () => {
  let component: ScrollCardMesasComponent;
  let fixture: ComponentFixture<ScrollCardMesasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrollCardMesasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollCardMesasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

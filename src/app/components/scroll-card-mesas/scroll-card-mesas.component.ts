import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scroll-card-mesas',
  templateUrl: './scroll-card-mesas.component.html',
  styleUrls: ['./scroll-card-mesas.component.css']
})
export class ScrollCardMesasComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  dirigir(){
    this.router.navigate(['dashboard/mesa']);
  }
}

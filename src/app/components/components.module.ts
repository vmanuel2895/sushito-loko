import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'

import { ChartsModule } from 'ng2-charts';

import { ModalImagenComponent } from './modal-imagen/modal-imagen.component';
import { ScrollCardMesasComponent } from './scroll-card-mesas/scroll-card-mesas.component';
import { DetalleOrdenesComponent } from './detalle-ordenes/detalle-ordenes.component';
import { DetalleMesaInfoComponent } from './detalle-mesa-info/detalle-mesa-info.component';
import { DetalleOrdenInfoComponent } from './detalle-orden-info/detalle-orden-info.component';
import { DetalleProductoComponent } from './detalle-producto/detalle-producto.component';
import { CarritoComponent } from './carrito/carrito.component';
import { PedidoMeseroComponent } from './pedido-mesero/pedido-mesero.component';
import { CajaMesasComponent } from './caja-mesas/caja-mesas.component';
import { CajaPedidosComponent } from './caja-pedidos/caja-pedidos.component';
import { CajaPedidosDomicilioComponent } from './caja-pedidos-domicilio/caja-pedidos-domicilio.component';


@NgModule({
  declarations: [
    ModalImagenComponent,
    ScrollCardMesasComponent,
    DetalleOrdenesComponent,
    DetalleMesaInfoComponent,
    DetalleOrdenInfoComponent,
    DetalleProductoComponent,
    CarritoComponent,
    PedidoMeseroComponent,
    CajaMesasComponent,
    CajaPedidosComponent,
    CajaPedidosDomicilioComponent
  ],
  exports: [
    ModalImagenComponent,
    ScrollCardMesasComponent,
    DetalleOrdenesComponent,
    DetalleMesaInfoComponent,
    DetalleOrdenInfoComponent,
    DetalleProductoComponent,
    CarritoComponent,
    PedidoMeseroComponent,
    CajaMesasComponent,
    CajaPedidosComponent,
    CajaPedidosDomicilioComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ]
})
export class ComponentsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleMesaInfoComponent } from './detalle-mesa-info.component';

describe('DetalleMesaInfoComponent', () => {
  let component: DetalleMesaInfoComponent;
  let fixture: ComponentFixture<DetalleMesaInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleMesaInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleMesaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

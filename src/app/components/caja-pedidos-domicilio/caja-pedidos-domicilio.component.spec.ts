import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaPedidosDomicilioComponent } from './caja-pedidos-domicilio.component';

describe('CajaPedidosDomicilioComponent', () => {
  let component: CajaPedidosDomicilioComponent;
  let fixture: ComponentFixture<CajaPedidosDomicilioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaPedidosDomicilioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaPedidosDomicilioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

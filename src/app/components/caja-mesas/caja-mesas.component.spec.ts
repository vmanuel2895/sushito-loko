import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaMesasComponent } from './caja-mesas.component';

describe('CajaMesasComponent', () => {
  let component: CajaMesasComponent;
  let fixture: ComponentFixture<CajaMesasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaMesasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaMesasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-caja-mesas',
  templateUrl: './caja-mesas.component.html',
  styleUrls: ['./caja-mesas.component.css']
})
export class CajaMesasComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  routingPage(){
    this.route.navigate(['/dashboard/informacion/pago']);
  }

}

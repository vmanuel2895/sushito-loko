import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginForm } from 'src/app/interfaces/login-form.interface';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { Usuario } from 'src/app/models/usuario.model';


const base_url = environment.apiUrlDev

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public usuario:Usuario;

  constructor(private _http: HttpClient) { }

  login(usuario:LoginForm){
    return this._http.post(`${base_url}/login`, usuario)
                     .pipe(
                        tap((resp:any)=>{
                          const { nombre } = resp.usuarioDB;
                          console.log(resp.usuarioDB);

                          /* this.usuario = new Usuario(nombre); */
                          localStorage.setItem('token', resp.token);
                        })
                      );
  }
}

import { Injectable } from '@angular/core';
import { sideBar }  from '../../interfaces/sidebar.interface';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  side:sideBar[]=[
    {
      name:'MESAS',
      img:'',
      route:'mesas'
    },
    {
      name:'MENÚ',
      img:'',
      route:'menu'
    },
    {
      name:'PEDIDOS',
      img:'',
      route:'pedidos'
    },
    {
      name:'PROMOCIONES',
      img:'',
      route:'promociones'
    },
    {
      name:'MESAS',
      img:'',
      route:'caja'
    },
    {
      name:'CORTES',
      img:'',
      route:'corte/caja'
    },
    {
      name:'EGRESOS',
      img:'',
      route:'egresos'
    },
    {
      name:'FONDO DE CAJA',
      img:'',
      route:'fondo'
    },
    {
      name:'PROPINAS',
      img:'',
      route:'propinas'
    },
    {
      name:'AGREGAR',
      img:'',
      route:'agregar/cliente'
    },
  ];

  constructor() { }

  getSideBar<sideBar>(){
    return this.side;
  }
}
